# Prerequisites

To participate in these labs, please ensure you have access to a machine with the software listed below.

> Note: up-to-date installation instructions for each of the following tools are available on their respective homepages. Unless stated otherwise, no further configuration is required.


## 1.0 Writing a 'Hello World' application in Java
If you would like to follow these labs end-to-end, and create an application from scratch, please ensure you have the following:

- [Java Development Kit](https://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html)
- [Gradle](https://gradle.org/install/)
- [Git](https://git-scm.com/downloads)

An Integrated Development Environment (IDE) is helpful. For Java development, [IntelliJ Community Edition](https://www.jetbrains.com/idea/download/#section=mac) is an excellent choice.

If you would prefer to containerise and orchestrate an application that you already have, you may skip this section.


## 2.0 Containerising a 'Hello World' application
In this section, we build a [Docker](https://www.docker.com) container image that we will later run on [Kubernetes](https://kubernetes.io), you will need the Docker Engine. The current desktop solution is:

- [Docker Desktop](https://www.docker.com/get-started)

> Note: requires Microsoft Windows 10 Professional or Enterprise 64-bit. For previous versions, get Docker Toolbox.  You will also be required to create a Docker Hub account.  The installation has dependencies on additional Windows features including Hyper-V and Containers. A restart will be required if these need to be enabled.

## 4.0 Creating an AKS Cluster
In this section, we provide two alternatives for Azure Kubernetes Service cluster creation:

- [Azure CLI](https://docs.microsoft.com/en-us/cli/azure/install-azure-cli-windows?view=azure-cli-latest)
- [Terraform](https://learn.hashicorp.com/terraform/getting-started/install.html)

Ensure that you have the necessary software installed before continuing.

## Next
In the next section, we will create a simple Spring Boot microservice with an HTTP endpoint (from scratch).

00 Prerequisites | [01 Writing a “Hello World” Application](01_I_create_hello_world_java.md) >