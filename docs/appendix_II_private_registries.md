# Appendix I - Private Registries

## Introduction
If using a private container registry, we need to supply Kubernetes with the appropriate secret so that it can authenticate.

## Creating a Deploy Token in Gitlab

- Visit _Settings > Repository > Deploy Tokens_.
- Provide a name, expiry date (if required) and the __read_registry__ scope
- After clicking _Create Deploy Token_, make sure to save the password

![Creating a Gitlab Deploy Token](img/appendix_gitlab_deploy_token.png "Gitlab Deploy Token")
![Active Deploy Tokens](img/appendix_active_deploy_tokens.png "Active Deploy Tokens")


## Registering the Token in Kubernetes

Using `kubectl`, register the secret.

> Note: it is important to register the secret in the same namespace in which it will be used (i.e. the same as the deployment).

Use the following:

| Kubernetes Field Name | Gitlab Field Name | 
|---|---|
| docker-server | Copy the Container Registry link in the Gitlab 'Registry' page, e.g. registry.gitlab.com/citihub/aks-accelerator |
| docker-username | Gitlab Deploy Token Name, e.g. gitlab+deploy-token-xxxxx | 
| docker-password | Gitlab Deploy Token password, only shown once during creation |
| docker-email | Your email address |
| namespace | The Kubernetes namespace in which the deployment will be created, e.g. aks-accelerator-xxxxxxxx |

```
➜ kubectl create secret docker-registry gitlab-reg-creds \
--docker-server=registry.gitlab.com/citihub/aks-accelerator \
--docker-username=gitlab+deploy-token-xxxxx \
--docker-password=xxxxxxxxxxxxxxxx \
--docker-email=your.email@example.com \
--namespace aks-accelerator-xxxxxxxx
```

## Using the Token in Kubernetes

Modify your deployment manifest to reference the new secret:

```
...
    spec:
      containers:
      - name: az-resource-service
        image: registry.gitlab.com/citihub/aks-accelerator:latest
        imagePullPolicy: Always
        ports:
        - containerPort: 80
      imagePullSecrets:
        - name: gitlab-reg-creds
```

> Note: that `imagePullSecrets` is part of the Pod Spec, not the Container Spec.

> Note: the `name` matches the name of the secret when created earlier, e.g.

```
➜  kubectl get secrets -n az-resource-service-xxxxxx
NAME                                                       TYPE                                  DATA   AGE
gitlab-reg-creds                                           kubernetes.io/dockerconfigjson        1      22m 
```

