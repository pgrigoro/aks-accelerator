# Appendix I - Kubernetes Networking

## Introduction

Kubernetes networking is perhaps the most complicated aspect of Kubernetes, and the implementation in AKS is no exception.

In this section we try to demystify some of the options that are available to a cluster administrator and how they interact.


## Basic Networking

![Basic Networking](img/appendix_basic_networking.png "Basic Networking")

## Advanced Networking

![Advanced Networking](img/appendix_advanced_networking.png "Advanced Networking")


## Available Configuration Options

| Azure CLI Field | Terraform Field | Azure Portal Field | Description | Example | Default |
| --- | --- | --- | --- | --- | --- |
| vnet-subnet-id  | vnet_subnet_id | Cluster subnet | ID of a subnet in an existing VNet into which to deploy the cluster. Must accommodate the nodes, pods, and all Kubernetes and Azure resources that might be provisioned in your cluster. | 10.10.10.0/24 | |
| network-plugin | network_plugin | Selected automatically when choosing _Basic_ or _Advanced_ | The Kubernetes network plugin to use | azure for (Advanced Networking) | kubenet |
| service-cidr | service_cidr | Kubernetes service address range | CIDR from which to assign service cluster IPs. MUST NOT overlap with any Subnet IP ranges. CIDR must be smaller than /12. | 192.168.0.0/16 | 10.0.0.0/16  | 
| dns-service-ip  | dns_service_ip | Kubernetes DNS Service IP address | An IP assigned to the Kubernetes DNS service (kube-dns). MUST be within the service address range, but not the first address (which is used for the kubernetes.default.svc.cluster.local address) | 192.168.0.10 | 10.0.0.10 | 
| docker-bridge-address | docker_bridge_cidr | Docker Bridge Address | CIDR for Docker Bridge. MUST NOT be in any Subnet IP ranges or service address range | 172.17.0.1/16 | 172.17.0.1/16 |  
| pod-cidr | pod_cidr | N/A  | CIDR from which to assign pod IPs when kubenet is used. MUST NOT overlap with any Subnet IP ranges. | 172.244.0.0/16 |   |
| network-policy | Not yet in TF | Not yet in portal | (PREVIEW) The Kubernetes network policy to use. Using together with "azure" network plugin. | azure or calico | "" (disabled) |  


## Further Reading

The following two Azure pages are useful when read together:

- [Network concepts for applications in Azure Kubernetes Service (AKS)](https://docs.microsoft.com/en-us/azure/aks/concepts-network)
- [Configure Azure CNI networking in Azure Kubernetes Service (AKS)](https://docs.microsoft.com/en-us/azure/aks/configure-azure-cni)