variable "location" {}

variable "project_name" {}

variable "existing_subnet_id" {}

variable "tags" {
  type = "map"

  default = {
    "terraform" = true
  }
}

variable "service_principal" {}
variable "service_principal_password" {}
variable "create_new_network" {}
